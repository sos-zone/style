(function () {
    'use strict';

    angular
        .module('app')
        .controller('UsersController', UsersController);

    UsersController.$inject = ['UserService', 'StyleService'];

    function UsersController(UserService, StyleService) {
        var vm = this;

        vm.users = [];
        vm.styles = [];
        vm.actualStyles = [];

        vm.getUsers = getUsers;
        vm.getAllStyles = getAllStyles;
        vm.activateAllStyles = activateAllStyles;


        activate();


        function getUsers() {
            UserService.getUsers()
                .then(function(users) {
                    vm.users = users;
                });
        }
        
        function activateAllStyles() {
            angular.forEach(vm.styles, function (style, key) {
                vm.actualStyles[style.id] = true;
            });
        }

        function getAllStyles() {
            return StyleService.getAllStyles()
                .then(function(styles){
                    vm.styles = styles;
                });
        }

        function activate() {
            vm.getUsers();
            vm.getAllStyles().then(function () {
                vm.activateAllStyles();
            });
            
        }
    }
}());