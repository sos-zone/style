(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserController', UserController);

    UserController.$inject = ['UserService', 'StyleService', '$stateParams'];

    function UserController(UserService, StyleService, $stateParams) {
        var vm = this;

        vm.user = {};
        vm.styles = [];

        vm.putUser = putUser;
        vm.updateUser = updateUser;
        vm.addUserStyle = addUserStyle;
        vm.removeUserStyle = removeUserStyle;

        
        activate();

        
        function activate() {
            UserService.getUser($stateParams.userId)
                .then(function(user) {
                    vm.user = user;
                });
            
            StyleService.getAllStyles()
                .then(function (styles) {
                    vm.styles = styles;
                });
        }
        
        function putUser() {
            UserService.putUser(vm.user)
                .then(function (user) {
                    vm.user = user;
                });
        }
        
        function updateUser() {
            UserService.getUser(vm.user.id)
                .then(function (user) {
                    vm.user = user;
                });
        }

        function addUserStyle(style) {
            UserService.addUserStyle(vm.user, style)
                .then(function () {
                    updateUser();
                });

        }
        
        function removeUserStyle(style) {
            UserService.removeUserStyle(vm.user, style)
                .then(function () {
                    updateUser();
                });
        }
    }
}());
