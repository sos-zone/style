(function () {
    'use strict';

    angular
        .module('app')
        .config(configureState);

    configureState.$inject = ['$stateProvider', '$urlRouterProvider'];

    /* @ngInject */
    function configureState($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('root', {
                url: '/',
                templateUrl: 'src/js/users/users.html',
                controller: 'UsersController',
                controllerAs: 'vm'
            })
            .state('user', {
                url: '/user/{userId}',
                templateUrl: 'src/js/users/user/user.html',
                controller: 'UserController',
                controllerAs: 'vm',
                
            });

        $urlRouterProvider.otherwise('/');
    }
})();