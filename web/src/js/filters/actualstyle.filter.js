(function () {
    'use strict';

    angular
        .module('app')
        .filter('ActualStyleFilter', ActualStyleFilter);

    function ActualStyleFilter() {
        return function(users, actualStyles){
            // console.log(users);
            // console.log(actualStyles);

            var result = [];

            if(typeof users=='undefined' || 0===users.length) {
                return users;
            }

            if(typeof actualStyles=='undefined' || 0===actualStyles.length) {
                return [];
            }

            angular.forEach(users, function(user, userKey){
                var keepGoing = true;
                angular.forEach(user.styles, function (userStyle, userStyleKey) {
                    if(keepGoing) {
                        angular.forEach(actualStyles, function(actualStyle, actualStyleKey){
                            if (userStyle.id == actualStyleKey && false !== keepGoing && true == actualStyle) {
                                result.push(user);
                                keepGoing = false;
                            }
                        });
                    }
                });
            });
            return result;
        };
    }
}());
