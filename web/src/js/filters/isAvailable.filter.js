(function () {
    'use strict';

    angular
        .module('app')
        .filter('isAvailableFilter', isAvailableFilter);

    function isAvailableFilter() {
        return function(styles, userStyles){
            var result = [];
            
            angular.forEach(styles, function (style, key) {
                var keepGoing = true;

                if (keepGoing) {
                    angular.forEach(userStyles, function (userStyle, key) {
                        if (style.id == userStyle.id && true == keepGoing) {
                            keepGoing = false;
                        }
                    });
                }

                if (keepGoing) {
                    result.push(style);
                }
            });

            return result;
        };
    }
}());
