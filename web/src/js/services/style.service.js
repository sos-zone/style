(function() {
    'use strict';

    angular
        .module('app')
        .factory('StyleService', StyleService);

    StyleService.$inject = ['$http'];

    function StyleService($http) {
        var service = {
            getAllStyles: getAllStyles
        };
        return service;

        /**
         * get all Styles
         * @returns array
         */
        function getAllStyles() {
            return $http({
                url: '/api/styles',
                method: 'GET'
            }).then(getComplete);
            
            function getComplete(response) {
                return response.data;
            }
        }
    }
}());
