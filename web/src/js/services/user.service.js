(function() {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];

    function UserService($http) {
        var service = {
            getUsers: getUsers,
            getUser: getUser,
            putUser: putUser,
            getStyles: getStyles,
            addUserStyle: addUserStyle,
            removeUserStyle: removeUserStyle
        };
        return service;

        /**
         * get all Users
         * @returns array
         */
        function getUsers() {
            return $http({
                url: '/api/users',
                method: 'GET'
            }).then(getComplete);
            
            function getComplete(response) {
                return response.data;
            }
        }

        /**
         * get single User
         * @returns {*}
         */
        function getUser(userId) {
            return $http({
                url: '/api/user/' + userId,
                method: 'GET'
            }).then(getComplete);

            function getComplete(response) {
                return response.data;
            }
        }

        /**
         * put User
         * @returns {*}
         */
        function putUser(user) {
            return $http({
                url: '/api/user/' + user.id,
                method: 'PUT',
                data: user
            }).then(getComplete);

            function getComplete(response) {
                return response.data;
            }
        }

        function getStyles(userId) {
            return $http({
                url: '/api/user/' + userId + '/styles',
                method: 'GET'
            })
                .then(getComplete);

            function getComplete(response) {
                return response.data;
            }
        }

        function addUserStyle(user, style) {
            return $http({
                url: '/api/user/' + user.id + '/style/' + style.id,
                method: 'PUT'
            })
                .then(function (response) {
                    return response.data;
                });
        }
        
        function removeUserStyle(user, style) {
            return $http({
                url: '/api/user/' + user.id + '/style/' + style.id,
                method: 'DELETE'
            })
                .then(function (response) {
                    return response.data;
                });
        }
    }
}());