<?php

namespace StyleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use StyleBundle\Entity\Style;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="StyleBundle\Repository\UserRepository")
 * @JMS\ExclusionPolicy("all")
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @JMS\Expose
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @JMS\Expose
     */
    private $email;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Style", inversedBy="users", cascade={"persist"})
     * @JMS\Expose
     * @MaxDepth(1)
     */
    private $styles;

    
    public function __construct()
    {
        $this->styles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add Style
     *
     * @param Style $style
     * @return User
     */
    public function addStyle($style)
    {
        if (!$this->getStyles()->contains($style)) {
            $this->styles->add($style);
        }

        return $this;
    }

    /**
     * Remove Style
     *
     * @param Style $style
     * @return User
     */
    public function removeStyle($style)
    {
        $this->styles->removeElement($style);

        return $this;
    }

    /**
     * Get Styles
     *
     * @return ArrayCollection
     */
    public function getStyles()
    {
        return $this->styles;
    }

}
