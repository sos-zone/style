<?php

namespace StyleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use StyleBundle\Entity\Style;

class StyleController extends Controller
{
    /**
     * Get Styles
     * @return mixed
     */
    public function getAllStylesAction()
    {
        $styles = $this->get('style.repository')->findAll();

        $serializer = $this->container->get('jms_serializer');
        $styles = $serializer->serialize($styles, 'json');

        return new Response($styles);
    }
}
