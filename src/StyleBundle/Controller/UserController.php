<?php

namespace StyleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use StyleBundle\Entity\User;
use StyleBundle\Entity\Style;

class UserController extends Controller
{
    /**
     * Get Users
     * @return mixed
     */
    public function getUsersAction()
    {
        $users = $this->get('user.repository')->findAll();

        $serializer = $this->container->get('jms_serializer');
        $users = $serializer->serialize($users, 'json');

        return new Response($users);
    }

    /**
     * Get Users
     * @param User $user
     * @return mixed
     * @ParamConverter("user", class="StyleBundle:User")
     */
    public function getUserAction(User $user)
    {
        $serializer = $this->container->get('jms_serializer');
        $user = $serializer->serialize($user, 'json');

        return new Response($user);
    }

    /**
     * put User
     * @param Request $request
     * @param User $user
     * @return mixed
     * @ParamConverter("user", class="StyleBundle:User")
     */
    public function putUserAction(Request $request, User $user)
    {
        $user->setName($request->get('name'));

        if ($user->getEmail() !== $request->get('email')) {
            if (count($this->get('user.repository')->findByEmail($request->get('email'))) > 0) {
                throw new \Exception('Email already exists');
            }  else {
                $user->setEmail($request->get('email'));
            }
        }

        $this->getDoctrine()->getManager()->flush();

        $serializer = $this->container->get('jms_serializer');
        $user = $serializer->serialize($user, 'json');

        return new Response($user);
    }

    /**
     * put User Style
     * @param User $user
     * @param Style $style
     * @return mixed
     * @ParamConverter("user", class="StyleBundle:User")
     * @ParamConverter("style", class="StyleBundle:Style")
     */
    public function putUserStyleAction(User $user, Style $style)
    {
        $user->addStyle($style);

        $this->getDoctrine()->getManager()->flush();
        
        return new Response();   
    }

    /**
     * remove user style
     * @param User $user
     * @param Style $style
     * @return mixed
     * @ParamConverter("user", class="StyleBundle:User")
     * @ParamConverter("style", class="StyleBundle:Style")
     */
    public function removeUserStyleAction(User $user, Style $style)
    {
        $user->removeStyle($style);
        
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new Response();
    }
}
